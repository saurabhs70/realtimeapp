//
//  MessageCell.swift
//  ChatApp
//
//  Created by saurabh-pc on 28/07/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet weak var lbcounter: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var sender: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setup(message:MessagesList)
    {
        let obj = message
        self.sender.text = obj.sender
       
        let   objmessage = obj.messages.last
        if let objm = objmessage
        {
            
             self.time.text = "Just Now"
            if((objm.isIncming == true))
            {
                if(MessageManager.sharedInstance.readmessages.contains(objm.time)   )
                {
                    self.lbcounter.isHidden = true
                    self.time.isHidden = true
                }
                else
                {
                    self.lbcounter.isHidden = false
                    self.time.isHidden = false
                }
            }
            else
            {
                self.lbcounter.isHidden = true
                self.time.isHidden = true
            }
            
        }
    }


}
