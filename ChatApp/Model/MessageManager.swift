//
//  MessageManager.swift
//  ChatApp
//
//  Created by saurabh-pc on 27/07/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//
protocol setupval {
    var didSelectItem: ((String) -> Void)? { get set }

}
import UIKit
import AudioToolbox

import PubNub
class MessageManager:NSObject,PNObjectEventListener,setupval {
    var client: PubNub!
    var owner:String = ""
    var readmessages = Set([NSNumber]()) //[NSNumber]()
    static let sharedInstance = MessageManager()
    var didSelectItem: ((String) -> Void)?
    var lists:[String] = [String]()
    var messages:[Message] = [Message]()
    var formatmessages:[MessagesList] = [MessagesList]()
   func  setupManager(UIId:String)
    {
        let configuration = PNConfiguration(publishKey: MESSAGE_PUBLISH_KEY, subscribeKey: MESSAGE_SUBSCRIBER_KEY)
        configuration.uuid = UIId
        configuration.presenceHeartbeatValue = 20
        configuration.presenceHeartbeatInterval = 10
        client = PubNub.clientWithConfiguration(configuration)
        owner = UIId
       setup()

    }
    func setup() {
        client.addListener(self)
        // Subscribe to demo channel with presence observation
        client.subscribeToChannels([MESSAGE_CHANNEL], withPresence: true)
    }
    func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {

        // Handle new message stored in message.data.message
        if message.data.channel != message.data.subscription {

            // Message has been received on channel group stored in message.data.subscription.
        }
        else {
            let message1 = message.data.message as! [String:String]
            var ids = [String]()
            if(owner == message1["reciver"])
            {
            let messageobj  = Message.init(_sender: message1["sender"] ?? "", _reciver: message1["reciver"] ?? "", _message: message1["message"] ?? "")
                messageobj.isIncming = true
                messageobj.time = message.data.timetoken
            messages.append(messageobj)
            
            for messagesObj in messages
            {
                //let messagesObj1 = messagesObj
                if(!ids.contains(messagesObj.sender))
                {
                   ids.append(messagesObj.sender)
                }
            }
                formatmessages.removeAll()
            for objids in ids
            {
            let arr =    messages.filter{$0.sender == objids }
                let messagefrmat = MessagesList.init(_sender: objids, _messages: arr)
                formatmessages.append(messagefrmat)
            }
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            }
            else if(owner == message1["sender"])
            {
                let messageobj  = Message.init(_sender: message1["reciver"] ?? "", _reciver: message1["sender"] ?? "", _message: message1["message"] ?? "")
                messageobj.isIncming = false
                messages.append(messageobj)
                
                for messagesObj in messages
                {
                    //let messagesObj1 = messagesObj
                    if(!ids.contains(messagesObj.sender))
                    {
                        ids.append(messagesObj.sender)
                    }
                }
                formatmessages.removeAll()
                for objids in ids
                {
                    let arr =    messages.filter{$0.sender == objids }
                    let messagefrmat = MessagesList.init(_sender: objids, _messages: arr)
                    formatmessages.append(messagefrmat)
                }
               // AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            }
            
            NotificationCenter.default.post(name: Notification.Name("updatemessagelist"), object: nil)
            // Message has been received on channel stored in message.data.channel.
        }

       // print("Received message: \(message.data.message) on channel \(message.data.channel) " +
            //"at \(message.data.timetoken)")
    }
    func client(_ client: PubNub, didReceive status: PNStatus) {
        
        if status.operation == .subscribeOperation {

            // Check whether received information about successful subscription or restore.
            if status.category == .PNConnectedCategory || status.category == .PNReconnectedCategory {

                let subscribeStatus: PNSubscribeStatus = status as! PNSubscribeStatus
                if subscribeStatus.category == .PNConnectedCategory {
                    didSelectItem?("loggedin")

                    //hearlist()
// sucess
                
                }
                else {

                    /**
                     This usually occurs if subscribe temporarily fails but reconnects. This means there was
                     an error but there is no longer any issue.
                     */
                }
            }
            else if status.category == .PNUnexpectedDisconnectCategory {

                /**
                 This is usually an issue with the internet connection, this is an error, handle
                 appropriately retry will be called automatically.
                 */
            }
                // Looks like some kind of issues happened while client tried to subscribe or disconnected from
                // network.
            else {

                let errorStatus: PNErrorStatus = status as! PNErrorStatus
                if errorStatus.category == .PNAccessDeniedCategory {

                    /**
                     This means that PAM does allow this client to subscribe to this channel and channel group
                     configuration. This is another explicit error.
                     */
                }
                else {

                    /**
                     More errors can be directly specified by creating explicit cases for other error categories
                     of `PNStatusCategory` such as: `PNDecryptionErrorCategory`,
                     `PNMalformedFilterExpressionCategory`, `PNMalformedResponseCategory`, `PNTimeoutCategory`
                     or `PNNetworkIssuesCategory`
                     */
                }
            }
        }
    }
    func hearlist()  {
        self.client.hereNowForChannel(MESSAGE_CHANNEL, withVerbosity: .UUID,
                                      completion: {[weak self] (result, status) in
                                        
                                        if status == nil {
                                            if let resulst = result
                                            {
                                                let arr = resulst.data.uuids as! [String]
                                                self?.lists = arr.filter{ $0 != self?.owner }
                                                //self?.lists = resulst.data.uuids as! [String]
                                                NotificationCenter.default.post(name: Notification.Name("updatelist"), object: nil)

                                          //  print(resulst.data.uuids,resulst.data.occupancy)
                                            }
                                            /**
                                             Handle downloaded presence information using:
                                             result.data.uuids - list of uuids.
                                             result.data.occupancy - total number of active subscribers.
                                             */
                                        }
                                        else {
                                            
                                            /**
                                             Handle presence audit error. Check 'category' property to find
                                             out possible reason because of which request did fail.
                                             Review 'errorData' property (which has PNErrorData data type) of status
                                             object to get additional information about issue.
                                             
                                             Request can be resent using: status.retry()
                                             */
                                        }
    })
}
    func sendMessage(message:Message)  {
        let dict = ["sender":message.sender,"reciver":message.reciver,"message":message.message]
        self.client.publish(dict, toChannel: MESSAGE_CHANNEL,
                            compressed: false, withCompletion: { (status) in
                                
                                if !status.isError {
                                    
                                    // Message successfully published to specified channel.
                                }
                                else{
                                    
                                    /**
                                     Handle message publish error. Check 'category' property to find
                                     out possible reason because of which request did fail.
                                     Review 'errorData' property (which has PNErrorData data type) of status
                                     object to get additional information about issue.
                                     
                                     Request can be resent using: status.retry()
                                     */
                                }
        })
    }
}
