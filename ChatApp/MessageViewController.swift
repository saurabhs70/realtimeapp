//
//  MessageViewController.swift
//  ChatApp
//
//  Created by saurabh-pc on 28/07/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit
import GrowingTextView
class MessageViewController: UIViewController {
    var message: MessagesList?
    var reciver:String = ""
    @IBOutlet weak var messagetxt: UITextView!
    @IBOutlet weak var tblist: UITableView!
    @IBOutlet weak var btmCostant: NSLayoutConstraint!
    @IBOutlet weak var viewgrowing: UIView!
    var messages:[Message] = [Message]()
    override func viewDidLoad() {
        super.viewDidLoad()
        messagetxt.delegate = self
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: Notification.Name.UIKeyboardDidHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updatemessage(notification:)), name: Notification.Name("updatemessagelist"), object: nil)

//        message = MessageManager.sharedInstance.formatmessages.filter{$0.sender == reciver}.last
//
//        if let message = message {
//            messages = message.messages
//            let arr = messages.filter{$0.isIncming == true}.flatMap{$0.time}
//           let arrset =   Set(arr)
//       MessageManager.sharedInstance.readmessages = MessageManager.sharedInstance.readmessages.union(arrset)
//        }
        updatevale()
        
    }
    func updatevale()  {
        message = MessageManager.sharedInstance.formatmessages.filter{$0.sender == reciver}.last
        
        if let message = message {
            messages = message.messages
            let arr = messages.filter{$0.isIncming == true}.flatMap{$0.time}
            let arrset =   Set(arr)
            MessageManager.sharedInstance.readmessages = MessageManager.sharedInstance.readmessages.union(arrset)
            tblist.reloadData()
        }

    }
    @objc func updatemessage(notification: Notification){
        DispatchQueue.main.async {[weak self] in
            //self?.message = MessageManager.sharedInstance.formatmessages.filter{$0.sender == self?.reciver}.last
            //if let message = self?.message {
             //  self?.messages = message.messages
                //  self?.tblist.reloadData()
            //}
         self?.updatevale()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //Mark :- ibaction
@IBAction func btnsendTapped(_ sender: Any) {
    if(messagetxt.text.count >= 1)
    {
        let message = Message.init(_sender: MessageManager.sharedInstance.owner, _reciver: reciver, _message: messagetxt.text)
        MessageManager.sharedInstance.sendMessage(message: message)
        messagetxt.text = nil
    }
    }

}
extension MessageViewController:UITableViewDelegate
{
    
}
extension MessageViewController:UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageViewCell", for: indexPath) as! MessageViewCell
        let messageobj = messages[indexPath.row]
        if (!messageobj.isIncming)
        {
            cell.backgroundColor = UIColor.green
        }
        else
        {
            cell.backgroundColor = UIColor.red
        }
        cell.message.text = messageobj.message
        return cell
    }
    
   
}
extension MessageViewController:GrowingTextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
    }
    func textViewDidEndEditing(_ textView: UITextView) {
    }
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            print("notification: Keyboard will show")
            btmCostant.constant = -250
//            if self.view.frame.origin.y == 0{
//                self.view.frame.origin.y -= keyboardSize.height
//            }
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            btmCostant.constant = 0

//            if self.view.frame.origin.y != 0 {
//                self.view.frame.origin.y += keyboardSize.height
//            }
        }
    }
}
