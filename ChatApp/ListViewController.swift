//
//  MessageViewController.swift
//  ChatApp
//
//  Created by saurabh-pc on 27/07/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    var userlists:[String] = [String]()
    var usermessages:[MessagesList] = [MessagesList]()
        @IBOutlet weak var tbllist: UITableView!
    var isMessage:Bool = false
    @IBAction func messageModeTapped(_ sender: Any) {
        let seg = sender as! UISegmentedControl
        if(seg.selectedSegmentIndex == 1)
        {
            isMessage = true
            usermessages = MessageManager.sharedInstance.formatmessages
            tbllist.reloadData()
        
        }
        else
        {
          isMessage = false
          tbllist.reloadData()

        }

    }
    @IBOutlet weak var messageMode: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        userlists = MessageManager.sharedInstance.lists
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("updatelist"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updatemessage(notification:)), name: Notification.Name("updatemessagelist"), object: nil)


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func methodOfReceivedNotification(notification: Notification){
        DispatchQueue.main.async {[weak self] in
            self?.userlists = MessageManager.sharedInstance.lists
            if(!(self?.isMessage)!)
              {
              self?.tbllist.reloadData()
              }

        }
            }
    @objc func updatemessage(notification: Notification){
        DispatchQueue.main.async {[weak self] in
            self?.usermessages = MessageManager.sharedInstance.formatmessages
            self?.tbllist.reloadData()
        }
    }

}
extension ListViewController:UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        if (!isMessage)
        {
//        let message = Message.init(_sender: MessageManager.sharedInstance.owner, _reciver: userlists[indexPath.row], _message: "string")
//        MessageManager.sharedInstance.sendMessage(message: message)
           // let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
           // let vc = storyboard.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
            //vc.message = usermessages[indexPath.row]
            vc.reciver = userlists[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
           
            let obj = usermessages[indexPath.row]
            vc.message = usermessages[indexPath.row]
            vc.reciver = obj.sender
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
}
extension ListViewController:UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(!isMessage)
        {
        return userlists.count
        }
        return usermessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (!isMessage)
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListNameCell", for: indexPath) as! ListNameCell
        cell.lblListName.text = userlists[indexPath.row]
        
        return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
            let obj = usermessages[indexPath.row]
            cell.setup(message: obj)
            return cell

    }
    
    
}
}
