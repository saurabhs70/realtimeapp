//
//  ViewController.swift
//  ChatApp
//
//  Created by saurabh-pc on 27/07/18.
//  Copyright © 2018 saurabh-pc. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {
    var messageManager : MessageManager?
    var timer:Timer?
//    lazy var vc:ListViewController = {
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
//        return vc
//    }()
    @IBOutlet weak var txtid: UITextField!
    @IBAction func btnLoginTapped(_ sender: Any) {
         let uiid = txtid.text
        if let uiid = uiid
        {
             if( uiid.count >= 5)
             {
            messageManager =    MessageManager.sharedInstance
                messageManager?.setupManager(UIId: uiid)
                messageManager?.didSelectItem = { [weak self] str in
                    print(str)
                    self?.push()
                }
            }
             else
             {
                showError()
            }
        }
        else
        {
            showError()
        }
    }
    func push() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
      let nav = UINavigationController.init()
        nav.viewControllers = [vc]
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = nav
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func  showError()  {
        let alert = UIAlertController.init(title: "Error!", message: "Please enter 5 char min!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        txtid.delegate = self
        self.timer = Timer.scheduledTimer(timeInterval: 5, target: self  , selector: #selector(self.updatecall), userInfo: nil, repeats: true)

        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func updatecall() {
    if let messageManager = messageManager
        {
      messageManager.hearlist()
        }
    }

}

